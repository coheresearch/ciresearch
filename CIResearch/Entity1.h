//
//  Entity1.h
//  CIResearch
//
//  Created by Maciej Szewczyk on 17/07/15.
//  Copyright (c) 2015 Cohesiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Entity1 : NSManagedObject

@property (nonatomic, retain) NSString * attribute1;

@end
